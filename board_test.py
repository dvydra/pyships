import unittest
from board import *
from ship import *
from cell_info import *

class BoardTest(unittest.TestCase):

    def setUp(self):
        self.board = Board(10)
        self.ac = AircraftCarrier()
        self.ac.set_ori('h')
        self.ac_v = AircraftCarrier()
        self.ac_v.set_ori('v')

    def test_size(self):
        self.assertEqual(self.board.size(),10)


    def test_add_ship(self):
        self.board.add_ship(self.ac, 1,1)
        self.assertEqual(self.board.at(1,1).ship,(self.ac))

    def test_no_ship(self):
        self.assertEqual(self.board.at(1,1).ship,None)

    def test_ship_horizontal(self):
        self.board.add_ship(self.ac, 1,1)
        self.assertEquals(self.board.at(2,1).ship,self.ac)

    def test_ship_vertical(self):
        self.board.add_ship(self.ac_v, 1,2)
        self.assertEquals(self.board.at(1,2).ship,self.ac_v)

    def test_ship_cord(self):
        self.board.add_ship(self.ac_v, 1,1)
        self.assertEquals(self.ac_v.coordinate(),(1,1))

    def test_miss(self):
        self.assertEquals(self.board.fire(1,2),'miss')
        cell = self.board.at(1,2)
        self.assertEquals(cell.ship,None)
        self.assertEquals(cell.fired,'miss')


    def test_hit(self):
        self.board.add_ship(self.ac, 1,1)
        self.assertEquals(self.board.fire(1,1),'hit')
        cell = self.board.at(1,1)
        self.assertEquals(cell.ship,self.ac)
        self.assertEquals(cell.fired,'hit')


    """def test_doublehit(self):
        self.assertEquals(self.board.double)"""



if __name__ == '__main__':
    unittest.main()