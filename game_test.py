import unittest
from game import *
from player import *
from board import *
from max import *
from dima import *

class GameTest(unittest.TestCase):
    def setUp(self):
        self.game = Game(Maxim(),Dima())

    def test_run(self):
        self.game.run()

#exploratory test
"""
    def test_choose_first(self):
        player1_chosen = 0
        player2_chosen = 0
        player1 = Maxim()
        player2 = Dima()
        number_tests = 10000
        for i in range(number_tests):
            game = Game(player1,player2)
            if game.player1 == player1:
                player1_chosen += 1
            else:
                player2_chosen += 1
        pct1 = float(player1_chosen) / number_tests
        pct2 = float(player2_chosen) / number_tests
        delta = abs(pct1 - pct2)
        print pct1
        print pct2
        print delta
        self.assertTrue(delta < 0.02, str(delta))
"""



if __name__ == '__main__':
    unittest.main()