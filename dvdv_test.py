import unittest
from game import *
from player import *
from board import *
from max import *
from dima import *

class DV1(Player):
    def __init__(self):
        Player.__init__(self,"DV1")

    def place_ships(self):
        ac = AircraftCarrier()
        ac.set_ori('h')
        self.board.add_ship(ac,1,1)

        bs = Battleship()
        bs.set_ori("v")
        self.board.add_ship(bs,2,3)

        cr = Cruiser()
        cr.set_ori("h")
        self.board.add_ship(cr,4,5)

    def go(self):
        if self.board.fire(1,1) == "hit":
            print "hit"

class DV2(Player):
    def __init__(self):
        Player.__init__(self,"DV2")

    def place_ships(self):
        ac = AircraftCarrier()
        ac.set_ori('h')
        self.board.add_ship(ac,1,1)

        bs = Battleship()
        bs.set_ori("v")
        self.board.add_ship(bs,2,3)

        cr = Cruiser()
        cr.set_ori("h")
        self.board.add_ship(cr,4,5)

    def go(self):
        if self.board.fire(1,1) == "hit":
            print "hit"





class DvDvTest(unittest.TestCase):
    def setUp(self):
        self.game = Game(DV1(),DV2())

    def test_run(self):
        self.game.run()

if __name__ == '__main__':
    unittest.main()