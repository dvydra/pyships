from cell_info import *

class Board(object):

    def __init__(self,size):
        self._size = size
        self.rows = []
        for i in range(0,self._size):
            row = []
            for j in range(0,self._size):
               row.append(CellInfo())
            self.rows.append(row)


    def size(self):
        return self._size

    def add_ship(self,ship,x,y):
        if ship.ori() == 'h':
            for i in range(0, ship.size()):
                self.rows[x+i-1][y-1].ship = ship
        elif ship.ori() == 'v':
            for i in range(0, ship.size()):
                self.rows[x-1][y+i-1].ship = ship
        ship.set_cord((x,y))

    def fire(self,x,y):

        if self.at(x,y).ship == None:
            cell = self.rows[x-1][y-1]
            cell.fired = 'miss'
            return 'miss'

        else:
            cell = self.rows[x-1][y-1]
            cell.fired = 'hit'
            return 'hit'

    def at(self,x,y):
        return self.rows[x-1][y-1]




