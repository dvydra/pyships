from board import *
from max import *
from dima import *
from Renderer import *
from player import *
from random import *

class Game(object):
    def __init__(self,player1,player2):

        self.choose_first_player(player1,player2)

    def choose_first_player(self,player1,player2):
        choice = randint(1,2)

        if choice == 1:
            self.player1 = player1
            self.player2 = player2

        else:
            self.player1 = player2
            self.player2 = player1

    def run(self):
        self.player1.set_board(Board(10))
        self.player2.set_board(Board(10))

        self.player1.place_ships()
        self.player2.place_ships()

        self.player1.go()

        renderer = Render(self.player1)
        renderer2 = Render(self.player2)
        renderer.print_board()
        renderer2.print_board()



if __name__ == '__main__':
    Game(Maxim(),Dima()).run()